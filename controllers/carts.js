const { StatusCodes } = require('http-status-codes');
const Cart = require('../models/Cart');
const Product = require('../models/Product');
const ShippingMethod = require('../models/ShippingMethod');

const cartJson = (id, products, total_price, shipping_method_id, user_id) => {
    return { id, products, total_price, shipping_method_id, user_id };
}

module.exports.getAllCarts = async (req, res) => {
    try {
        const carts = await Cart.find();
        return res.json({
            data: carts.map((cart) => {
                const products = cart.products
                    .map((val) => {
                        return {
                            id: val._id,
                            qty: val.qty
                        };
                });
                return cartJson(
                    cart.id, 
                    products, 
                    cart.total_price, 
                    cart.shipping_method_id, 
                    cart.user_id
                );
            })
        });
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}

module.exports.AddCart = async (req, res) => {
    // check if the json body is empty
    if (Object.keys(req.body).length === 0) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "Content cannot be empty"
        });
    }
    
    if (req.body.shipping_method_id === undefined) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: `shipping method can't be empty`
        });
    }

    if (req.body.user_id !== undefined && req.body.user_id !== req.user.id) {
        return res.status(StatusCodes.FORBIDDEN).json({
            message: "You can't add other user cart"
        });
    }


    if (req.body.total_price < 0) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "price must be greater than or equal to zero"
        });
    }

    const products = req.body.products;
    var total_price = 0;
    try {
        const user_id = req.user.id;
        let findCart = await Cart.findOne({ user_id: user_id });
        if (findCart !== null) {
            return res.status(StatusCodes.CONFLICT).json({
                message: "user already has cart"
            });
        }

        let shipping_method = await ShippingMethod.findById(req.body.shipping_method_id);
        if (shipping_method === null) {
            return res.status(StatusCodes.BAD_REQUEST).json({
                message: `Can't find the shipping method with id '${req.body.shipping_method_id}'`
            });
        }
        
        // sum total price in products
        let list_product_id = [];
        for (const key in products) {
            const product_req = products[key];
            if (product_req.qty <= 0) {
                return res.status(StatusCodes.BAD_REQUEST).json({
                    message: "quantity of a product must greater than zero"
                });
            }

            // change id to _id
            if (product_req.id !== undefined && product_req._id === undefined) {
                products[key]._id = products[key].id;
            }
            const id = product_req._id;

            // check if there are any double id in the products array
            if (list_product_id.includes(id)) {
                return res.status(StatusCodes.BAD_REQUEST).json({
                    message: "You have double product in the cart"
                });
            }
            list_product_id.push(id);

            const product = await Product.findById(id);
            if (product === null) {
                return res.status(StatusCodes.BAD_REQUEST).json({
                    message: `Can't find the product with id '${id}'`
                });
            }
            if (product_req.qty > product.stock) {
                return res.status(StatusCodes.CONFLICT).json({
                    message: `The quantity of the product with the name '${product_req.title}' and id '${id}' exceeds the available stock`,
                    qty: product_req.qty,
                    stock: product.stock
                });
            }
            total_price += product.price * product_req.qty;
        }
        
        // if req.body.total_price is not undefined
        if (req.body.total_price !== undefined && req.body.total_price !== total_price) {
            return res.status(StatusCodes.CONFLICT).json({
                message: `The products total price in the cart with id '${id}' doesn't match with the total price requested`
            });
        }
        req.body.total_price = total_price;
        
        req.body.user_id = req.user.id;
        let cart = await Cart.create(req.body);
        const products_cart = cart.products.map((val) => {
            return {
                id: val._id,
                qty: val.qty
            };
        });
        return res.status(StatusCodes.CREATED).json(
            cartJson(
                cart.id, 
                products_cart,
                cart.total_price, 
                cart.shipping_method_id, 
                cart.user_id
            )
        );
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}


module.exports.updateCartById = async (req, res) => {
    // check if the json body is empty
    if (Object.keys(req.body).length === 0) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "Content cannot be empty"
        });
    }

    if (req.body.id !== undefined || req.body._id !== undefined) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "id cart cannot be changed or updated"
        });
    }

    if (req.body.user_id !== undefined) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "user_id cannot be changed or updated"
        });
    }

    if (req.body.total_price < 0) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "price must be greater than or equal to zero"
        });
    }
    
    const id = req.params.id;
    const products_req = req.body.products;
    var total_price = 0;
    try {
        if (req.body.shipping_method_id !== undefined) {
            let shipping_method = await ShippingMethod.findById(req.body.shipping_method_id);
            if (shipping_method === null) {
                return res.status(StatusCodes.BAD_REQUEST).json({
                    message: `Can't find the shipping method with id '${req.body.shipping_method_id}'`
                });
            }
        }

        // sum total price in products
        let list_product_id = [];
        for (const key in products_req) {
            const product_req = products_req[key];
            if (product_req.qty <= 0) {
                return res.status(StatusCodes.BAD_REQUEST).json({
                    message: "quantity of a product must be greater than zero"
                });
            }

            // change id to _id
            if (product_req.id !== undefined && product_req._id === undefined) {
                product_req._id = product_req.id;
            }

            const id = product_req._id;

            // check if there are any double id in the products array
            if (list_product_id.includes(id)) {
                return res.status(StatusCodes.BAD_REQUEST).json({
                    message: "You have double product in the cart"
                });
            }
            list_product_id.push(id);

            const product = await Product.findById(id);
            if (product === null) {
                return res.status(StatusCodes.BAD_REQUEST).json({
                    message: `Can't find the product with id '${id}'`
                });
            }
            if (product_req.qty > product.stock) {
                return res.status(StatusCodes.CONFLICT).json({
                    message: `The quantity of the product with the name '${product_req.title}' and id '${id}' exceeds the available stock`,
                    qty: product_req.qty,
                    stock: product.stock
                });
            }
            total_price += product.price * product_req.qty;
        }
        
        // if req.body.total_price is not undefined
        if (req.body.total_price !== undefined && req.body.total_price !== total_price) {
            return res.status(StatusCodes.CONFLICT).json({
                message: `The products total price in the cart with id '${id}' doesn't match with the total price requested`
            });
        }
        req.body.total_price = total_price;

        let result = await Cart.updateOne({ _id: id, user_id: req.user.id }, req.body);
        if (result.matchedCount <= 0) {
            return res.status(StatusCodes.NOT_FOUND).json({
                message: `Cart with id '${id}' and user_id '${req.user.id}' not found`
            });
        }
        
        let cart = await Cart.findById(id);
        const products_res = cart.products.map((val) => {
            return {
                id: val._id,
                qty: val.qty
            };
        });
        return res.json({
            id: cart.id,
            products: products_res,
            shipping_method: cart.shipping_method_id
        });
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message,
        });
    }
}

module.exports.getCartById = async (req, res) => {
    const id = req.params.id;

    // get cart by id from database    
    try {
        const cart = await Cart.findById(id);
        if (cart === null) {
            return res.status(StatusCodes.NOT_FOUND).json({
                message: `Cart with id '${id}' not found`
            });
        }
        if (cart.user_id.toString() !== req.user.id) {
            return res.status(StatusCodes.FORBIDDEN).json({
                message: "You can't get other user cart"
            });
        }
        const products = cart.products.map((val) => {
            return {
                id: val._id,
                qty: val.qty
            };
        });
        return res.json(
            cartJson(
                cart.id, 
                products, 
                cart.total_price, 
                cart.shipping_method_id, 
                cart.user_id
            )
        );
        
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message,
        });
    }
}

module.exports.deleteCartById = async (req, res) => {
    const id = req.params.id;
    try {
        let result = await Cart.deleteOne({ _id: id, user_id: req.user.id });
        if (result.deletedCount <= 0) {
            return res.status(StatusCodes.NOT_FOUND).json({
                message: `Cart with id '${id}' and user_id '${req.user.id}' not found`
            });
        }
        return res.status(StatusCodes.NO_CONTENT).json({});
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}


module.exports.getCartByUserId = async (req, res) => {
    const userId = req.user.id;
    // get cart by userId from database    
    try {
        const cart = await Cart.findOne({ user_id: userId });
        if (cart) {
            const products = cart.products.map((val) => {
                return {
                    id: val._id,
                    qty: val.qty
                };
            });
            return res.json(
                cartJson(
                    cart.id, 
                    products, 
                    cart.total_price, 
                    cart.shipping_method_id, 
                    cart.user_id
                )
            );
        }
        return res.status(StatusCodes.NOT_FOUND).json({
            message: `Cart with userId '${userId}' not found`
        });
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}
