const { StatusCodes } = require('http-status-codes');
const ShippingMethod = require('../models/ShippingMethod');

const shippingMethodJson = (id, title) => {
    return { id, title };
}

module.exports.getAllShippingMethods = async (req, res) => {
    try {
        const shippingMethods = await ShippingMethod.find();
        return res.json({
            data: shippingMethods.map((shippingMethod) => {
                return shippingMethodJson(shippingMethod.id, shippingMethod.title);
            })
        });
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}

module.exports.getShippingMethodById = async (req, res) => {
    const id = req.params.id;

    // get cart by id from database    
    try {
        const shippingMethod = await ShippingMethod.findById(id);
        // console.log(shippingMethod);
        if (shippingMethod) {
            return res.json(
                shippingMethodJson(shippingMethod.id, shippingMethod.title)
            );
        }
        return res.status(StatusCodes.NOT_FOUND).json({
            message: `ShippingMethod with id '${id}' not found`
        });
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}