const { StatusCodes } = require('http-status-codes');
const Product = require('../models/Product');
const Category = require('../models/Category');

const productJson = (id, title, stock, price, description, image, category_id) => {
    return { id, title, stock, price, description, image, category_id };
}

module.exports.getProductById = async (req, res) => {
    const id = req.params.id;

    // get product by id from database
    try {
        const product = await Product.findById(id).exec();
        if (product) {
            // const product = products[0];
            return res.json(
                productJson(
                    product.id,
                    product.title,
                    product.stock,
                    product.price,
                    product.description,
                    product.image,
                    product.category_id
                )
            );
        }
        return res.status(StatusCodes.NOT_FOUND).json({
            "message": `Product with id '${id}' not found`
        });
        
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}

module.exports.getAllProducts = async (req, res) => {
    // get all product from database
    try {
        const products = await Product.find({});
        return res.json({
            data: products.map((product) => {
                return productJson(
                    product.id,
                    product.title,
                    product.stock,
                    product.price,
                    product.description,
                    product.image,
                    product.category_id
                );
            })
        });
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}

module.exports.getProductsByCategory = async (req, res) => {
    const category = req.params.category;

    // get all product by category from database
    try {
        const categoryWithId = await Category.find({ title: category }).exec();
        if (categoryWithId.length > 0) {
            const products = await Product.find({
                category_id: categoryWithId[0]._id
            }).exec();
            return res.json({
                data: products.map((product) => {
                    return productJson(
                        product.id, 
                        product.title, 
                        product.stock, 
                        product.price, 
                        product.description, 
                        product.image,
                        product.category_id
                    );
                })
            });
        }
        return res.json({ data: [] });

    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}


module.exports.getProductsCategories = async (req, res) => {
    // get all category from database 
    try {
        const categories = await Category.find({}).exec();
        return res.json(categories.map((e) => e.title));

    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}