require('dotenv').config();
const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { ReasonPhrases, StatusCodes } = require('http-status-codes');

function createAccessToken(id, email, hash_password, username) {
    const user = { id, email, hash_password, username };
    return jwt.sign(
        user, 
        process.env.ACCESS_TOKEN_SECRET, 
        { expiresIn: process.env.ACCESS_TOKEN_EXPIRESIN }
    );
}

function createRefreshToken(id, email, hash_password, username) {
    const user = { id, email, hash_password, username };
    return jwt.sign(
        user, 
        process.env.REFRESH_TOKEN_SECRET, 
        { expiresIn: process.env.REFRESH_TOKEN_EXPIRESIN }
    );
}

function accessTokenJson(accessToken) {
    return {
        accessToken: accessToken,
        token_type: "Bearer",
        expires_in: 86400
    };
}


module.exports.authUserByEmail = async (req, res, next) => {
    // check if the json body is empty
    if (Object.keys(req.body).length === 0) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "Content cannot be empty"
        });
    }

    const email = req.body.email;
    const password = req.body.password;

    if (email === undefined || email === "") return res.status(StatusCodes.BAD_REQUEST).json({
        message: "email field cannot be empty"
    });
    if (password === undefined || password === "") return res.status(StatusCodes.BAD_REQUEST).json({
        message: "password field cannot be empty"
    });
    
    try {
        const rows = await User.find({ email }).exec();
        if (rows.length <= 0) {
            return res.status(StatusCodes.UNAUTHORIZED).json({
                message: "email and password doesn't match"
            });
        }
        const user = rows[0];
        const isSame = await bcrypt.compare(password, user.hash_password);
        if (!isSame) {
            return res.status(StatusCodes.UNAUTHORIZED).json({
                message: "email and password doesn't match"
            });
        }
        req.user = user;
        return next();

    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
        
}

module.exports.authUserByUsername = async (req, res, next) => {
    // check if the json body is empty
    if (Object.keys(req.body).length === 0) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "Content cannot be empty"
        });
    }

    const username = req.body.username;
    const password = req.body.password;

    if (username === undefined || username === "") return res.status(StatusCodes.BAD_REQUEST).json({
        message: "username field cannot be empty"
    });
    if (password === undefined || username === "") return res.status(StatusCodes.BAD_REQUEST).json({
        message: "password field cannot be empty"
    });

    try {
        const rows = await User.find({ username }).exec();
        if (rows.length <= 0) {
            return res.status(StatusCodes.UNAUTHORIZED).json({
                message: "username and password doesn't match"
            });
        }

        const user = rows[0];
        const isSame = await bcrypt.compare(password, user.hash_password);
        if (!isSame) {
            return res.status(StatusCodes.UNAUTHORIZED).json({
                message: "username and password doesn't match"
            });
        }
        req.user = user;
        return next();
        
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}


module.exports.getTokenAfterLogin = async (req, res, next) => {
    const { _id, email, username, hash_password, image } = req.user;
    const id = _id.toString();

    try {
        const accessToken = createAccessToken(id, email, hash_password, username);
        const refreshToken = createRefreshToken(id, email, hash_password, username);

        res.cookie('jwt', refreshToken, { 
            httpOnly: true, sameSite: 'None',
            secure: true, maxAge: 24 * 60 * 60 * 1000 
        });
     
        return res.json({
            user: {
                id, email, username, image,
            },
            token: accessTokenJson(accessToken),
        });
    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}

module.exports.getAccessToken = async (req, res) => {
    if (!req.cookies?.jwt) return res.status(StatusCodes.NOT_ACCEPTABLE).json({ message: 'Unauthorized' });

    const refreshToken = req.cookies.jwt;

    if (refreshToken == null) {
        return res.status(StatusCodes.NOT_ACCEPTABLE).json({
            message: 'Unauthorized'
        });
    }

    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, async (err, user) => {
        if (err) {
            console.log(err);
            return res.status(StatusCodes.FORBIDDEN).json({
                message: ReasonPhrases.FORBIDDEN
            });
        }
        try {
            const accessToken = createAccessToken(user.id, user.email, user.hash_password, user.username);
    
            return res.json(accessTokenJson(accessToken));   
        } catch (error) {
            return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
                message: error.message
            });
        }
    });
}

module.exports.register = async (req, res) => {
    // check if the json body is empty
    if (Object.keys(req.body).length === 0) {
        return res.status(StatusCodes.BAD_REQUEST).json({
            message: "Content cannot be empty"
        });
    }

    const email = req.body.email;
    const password = req.body.password;
    const username = req.body.username;
    const image = req.body.image;
    const telp_no = req.body.telp_no;

    if (email === undefined || email === "") return res.status(StatusCodes.BAD_REQUEST).json({
        message: "email field cannot be empty"
    });
    if (password === undefined || password === "") return res.status(StatusCodes.BAD_REQUEST).json({
        message: "password field cannot be empty"
    });
    if (username === undefined || username === "") return res.status(StatusCodes.BAD_REQUEST).json({
        message: "username field cannot be empty"
    });
    
    try {
        const rowsEmail = await User.findOne({ email }).exec();
        if (rowsEmail) {
            return res.status(StatusCodes.CONFLICT).json({
                message: "email already exists"
            });
        }

        const rowsUsername = await User.findOne({ username }).exec();
        if (rowsUsername) {
            return res.status(StatusCodes.CONFLICT).json({
                message: "username already exists"
            });
        }

        const hash_password = await bcrypt.hash(password, 12);
        const result = await User.create({ email, hash_password, username, image, telp_no });
        return res.status(StatusCodes.CREATED).json({
            id: result.id,
            email: result.email,
            username: result.username,
            image: result.image,
            telp_no: result.telp_no
        });

    } catch (error) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: error.message
        });
    }
}