const express = require('express');
const router = express.Router();

// Controller
const cartsController = require('../controllers/carts');

router.route('/')
    .get(cartsController.getAllCarts)
    .post(cartsController.AddCart);

// Get Single Cart by UserId
router.route('/userid')
    .get(cartsController.getCartByUserId);

// Get Single Cart
router.route('/:id')
    .get(cartsController.getCartById)
    .patch(cartsController.updateCartById)
    .delete(cartsController.deleteCartById);
    
module.exports = router; 