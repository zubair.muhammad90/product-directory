const express = require('express');
const router = express.Router();
const productsController = require('../../controllers/products');

// Get Categories
router.route('/:category').get(productsController.getProductsByCategory);

module.exports = router;