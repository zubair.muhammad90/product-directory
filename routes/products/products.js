const express = require('express');
const router = express.Router();

// Route
const categoryRoute = require('./category');

// Controller
const productsController = require('../../controllers/products');


// Get All Product
router.route('/').get(productsController.getAllProducts);

// Get Products Categories
router.route('/categories').get(productsController.getProductsCategories);

// Category Route
router.use('/category', categoryRoute);

// Get Single Product
router.route('/:id').get(productsController.getProductById);


module.exports = router;