const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth');

router.route('/loginByEmail').post(authController.authUserByEmail, authController.getTokenAfterLogin);
router.route('/loginByUsername').post(authController.authUserByUsername, authController.getTokenAfterLogin);
router.route('/register').post(authController.register);
router.route('/token').post(authController.getAccessToken);

module.exports = router;