const express = require('express');
const router = express.Router();

// Controller
const shippingMethodsController = require('../controllers/shippingMethods');

// Get All Shipping Method
router.route('/')
    .get(shippingMethodsController.getAllShippingMethods);

// Get Single Shipping Method
router.route('/:id')
    .get(shippingMethodsController.getShippingMethodById);

module.exports = router;