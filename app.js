require('dotenv').config();
const { StatusCodes } = require('http-status-codes');
const mongoose = require('mongoose');
const connectDB = require('./config/dbConn');
const express = require('express');
const app = express();

const cookieParser = require('cookie-parser');
const cors = require('cors');

const { authToken } = require('./middlewares/auth');
const authRoute = require('./routes/auth');
const productsRoute = require('./routes/products/products');
const cartsRoute = require('./routes/carts');

const shippingMethodsRoute = require('./routes/shippingMethods');

const PORT = process.env.PORT;

// Connect to MongoDB
connectDB();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use('/api/v1/auth', authRoute);
app.use('/api/v1/products', authToken, productsRoute);
app.use('/api/v1/carts', authToken, cartsRoute);
app.use('/api/v1/shippingMethods', authToken, shippingMethodsRoute);

app.use((req, res) => {
    res.status(StatusCodes.NOT_FOUND);
    if (req.accepts('html')) {
        res.render('404');
    } else if (req.accepts('json')) {
        res.json({
            "message": "404 Not Found"
        });
    } else {
        res.type('txt').send("404 Not Found");
    }
});

mongoose.connection.once('open', () => {
    console.log("Connected to MongoDB");
    app.listen(PORT, () => {
        console.log(`Server running on port ${PORT}`);
    });
});
