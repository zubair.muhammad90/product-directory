const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
    },
    hash_password: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
        unique: true,
    },
    telp_no: {
        type: String,
    },
    image: {
        type: String,
    },
});

module.exports = mongoose.model('User', userSchema);