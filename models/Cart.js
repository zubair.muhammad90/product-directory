const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cartSchema = new Schema({
    products: {
        type: [{ 
            _id: {
                type: mongoose.Types.ObjectId,
                required: true,
                unique: true,
                ref: "Product",
            },
            qty: {
                type: Number, 
                required: true,
                min: 1,
            },
        }],
        default: [],
    },
    total_price: {
        type: Number,
        default: 0,
        min: 0,
    },
    shipping_method_id: {
        type: mongoose.Types.ObjectId,
        ref: "ShippingMethod",
        required: true,
    },
    user_id: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
});

module.exports = mongoose.model('Cart', cartSchema);