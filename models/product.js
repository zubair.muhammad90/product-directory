const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    stock: {
        type: Number,
        default: 0,
        min: 0,
    },
    price: {
        type: Number,
        default: 0,
        min: 0,
    },
    description: {
        type: String,
        default: ""
    },
    image: {
        type: String,
    },
    category_id: {
        type: mongoose.Types.ObjectId,
        ref: "Category",
        required: true,
    },
});

module.exports = mongoose.model('Product', productSchema);