const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const shippingMethodSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('ShippingMethod', shippingMethodSchema);