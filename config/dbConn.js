const mongoose = require('mongoose');

const connectDB = async () => {
    try {
        var url = 'mongodb://localhost:27017/product_directory';
        await mongoose.connect(url, {
            useUnifiedTopology: true,
            useNewUrlParser: true
        });

    } catch (error) {
        console.error(error);
    }
}

module.exports = connectDB;